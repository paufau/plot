import networkx as nx
import matplotlib.pyplot as plt

from plot import Plot


class Painter:
    G = nx.DiGraph()
    options = {
        'with_labels': True,
    }
    styles = {
        'node_color': 'blue',
        'node_size': 400,
        'font_weight': 'bold',
        'font_color': 'white'
    }

    def __init__(self, plot: Plot):
        self.G.add_nodes_from(list(node.name for node in plot.nodes))
        for iedge in plot.edges:
            self.G.add_edge(iedge.start, iedge.end, weight=iedge.weight)

    def draw_graph(self):
        pos = nx.spring_layout(self.G)
        nx.draw(self.G, pos=pos, **self.options, **self.styles)
        edge_labels = dict([((u, v,), d['weight'])
                            for u, v, d in self.G.edges(data=True)])
        nx.draw_networkx_edge_labels(self.G, pos, edge_labels=edge_labels, label_pos=0.3, font_size=7)
        plt.show()

    def save_graph(self, name="graph.png"):
        pos = nx.spring_layout(self.G)
        nx.draw(self.G, pos=pos, **self.options, **self.styles)
        edge_labels = dict([((u, v,), d['weight'])
                            for u, v, d in self.G.edges(data=True)])
        nx.draw_networkx_edge_labels(self.G, pos, edge_labels=edge_labels, label_pos=0.3, font_size=7)
        plt.savefig(name)
