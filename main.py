from painter import Painter
from plot import Node, Edge, Plot

nodes = [Node("a"), Node("b"), Node("c"), Node("d"), Node("e")]
edges = [
    Edge("a", "b", 1),
    Edge("b", "c", 2),
    Edge("c", "e", 4),
    Edge("e", "d", 5),
    Edge("d", "b", 1),
    Edge("c", "d", 2),
    Edge("d", "c", 3)
]
plot_obj = Plot(nodes, edges)

paths = plot_obj.find_shortest_path(Node('a'), Node('d'))
print(list( node.name for node in paths.nodes))
print(paths.length)

painter = Painter(plot_obj)
painter.draw_graph()
