from typing import List, Union

from plot import Node, Edge
from plot.path import Path


class Pathfinder(object):
    nodes: List[Node]
    edges: List[Edge]

    paths_found: List[Path]
    current_path: Path

    def __init__(self, nodes: List[Node], edges: List[Edge]):
        self.nodes = nodes
        self.edges = edges
        self.paths_found = []
        self.current_path = Path()

    def find_shortest_by_weight(self, _start: Node, _end: Node) -> Union[Path, None]:
        self.find_paths(_start, _end)
        if len(self.paths_found) > 0:
            shortest = self.paths_found[0]
            for ipath in self.paths_found:
                if ipath.length < shortest.length:
                    shortest = ipath
            return shortest
        return

    def find_shortest_by_nodes(self, _start: Node, _end: Node) -> Union[Path, None]:
        self.find_paths(_start, _end)
        if len(self.paths_found) > 0:
            shortest = self.paths_found[0]
            for ipath in self.paths_found:
                if len(shortest.nodes) > len(ipath.nodes):
                    shortest = ipath
            return shortest
        return

    def find_paths_except_nodes(self, _start, _end, _except_nodes) -> List[Path]:
        self.find_paths(_start, _end)
        paths: List[Path] = []
        for ipath in self.paths_found:
            for inode in _except_nodes:
                if str(inode.name) not in list(str(ipath_node.name) for ipath_node in ipath.nodes):
                    paths.append(ipath)
        return paths

    def find_paths(self, _start: Node, _end: Node, _length: int = 0) -> None:
        if _start.name == _end.name:
            self.current_path.nodes.append(_start)
            self.current_path.length += _length
            self.paths_found.append(Path(list(self.current_path.nodes), self.current_path.length))
            del self.current_path.nodes[-1]
            self.current_path.length -= _length
            return

        next_paths: List[Edge] = self.get_next_paths(_start)
        if len(next_paths) == 0:
            return

        for ipath in next_paths:
            if ipath.start not in list(inode.name for inode in self.current_path.nodes):
                self.current_path.nodes.append(_start)
                self.current_path.length += _length
                self.find_paths(Node(ipath.end), _end, ipath.weight)
                del self.current_path.nodes[-1]
                self.current_path.length -= _length
        return

    def get_next_paths(self, _node: Node) -> List[Edge]:
        next_paths: List[Edge] = []
        for iedge in self.edges:
            if iedge.start == _node.name:
                next_paths.append(iedge)
        return next_paths

    def _print_paths_found(self):
        print(list(list(inode.name for inode in ipath.nodes) for ipath in self.paths_found))
