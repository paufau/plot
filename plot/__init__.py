from typing import List

from plot.edge import Edge
from plot.node import Node
from plot.path import Path
from plot.pathfinder import Pathfinder


class Plot(object):
    edges: List[Edge] = []
    nodes: List[Node] = []

    def __init__(self, nodes: List[Node], edges: List[Edge]):
        self.edges = edges
        self.nodes = nodes

    def insert_node(self, _node: Node) -> None:
        self.nodes.append(_node)

    def remove_node(self, _node: Node) -> None:
        element_index = (list(inode.name for inode in self.nodes).index(_node.name))
        self.nodes.pop(element_index)

    def insert_edge(self, _edge: Edge) -> None:
        self.edges.append(_edge)

    def remove_edge(self, _edge: Edge) -> None:
        element_index = (list((iedge.start, iedge.end, iedge.weight) for iedge in self.edges).index(
            (_edge.start, _edge.end, _edge.weight)))
        self.edges.pop(element_index)

    # Степени вершин
    def node_exponents_list(self) -> List[int]:
        exponents: List[int] = []
        for inode in self.nodes:
            node_exponent: int = 0
            for iedge in self.edges:
                if inode.name == iedge.start:
                    node_exponent += 1
            exponents.append(node_exponent)
        exponents.sort(reverse=True)
        return exponents

    def find_paths(self, start: Node, end: Node) -> List[Path]:
        finder = Pathfinder(self.nodes, self.edges)
        finder.find_paths(start, end)
        return finder.paths_found

    def find_shortest_path(self, start: Node, end: Node) -> Path:
        finder = Pathfinder(self.nodes, self.edges)
        return finder.find_shortest_by_weight(start, end)

    def find_paths_except_nodes(self, start: Node, end: Node, except_nodes: List[Node]) -> List[Path]:
        finder = Pathfinder(self.nodes, self.edges)
        return finder.find_paths_except_nodes(start, end, except_nodes)

    def radius(self) -> int:
        distances_matrix: List[List[int]] = self.get_distances_matrix()
        shortest_distance = distances_matrix[0][1]
        for line in distances_matrix:
            for num in line:
                if num < shortest_distance and num != 0:
                    shortest_distance = num
        return shortest_distance

    def diameter(self) -> int:
        distances_matrix: List[List[int]] = self.get_distances_matrix()
        longest_distance = 0
        for line in distances_matrix:
            for num in line:
                if num > longest_distance:
                    longest_distance = num
        return longest_distance

    def get_distances_matrix(self) -> List[List[int]]:
        reversed_edges: List[Edge] = []
        for iedge in self.edges:
            reversed_edges.append(Edge(iedge.end, iedge.start, iedge.weight))

        paths: List[List[int]] = []

        for index_start, inode_start in enumerate(self.nodes):
            paths.append([])
            for index_end, inode_end in enumerate(self.nodes):
                paths[index_start].append(0)
                if index_start == index_end:
                    paths[index_start][index_end] = 0
                else:
                    finder = Pathfinder(self.nodes, self.edges + reversed_edges)
                    shortest = finder.find_shortest_by_nodes(inode_start, inode_end)
                    paths[index_start][index_end] = len(shortest.nodes) - 1
        return paths
