from typing import Union


class Node:
    name: Union[str, int] = ""

    def __init__(self, name: Union[str, int]):
        self.name = name
