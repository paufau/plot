from typing import List

from plot import Node


class Path:
    nodes: List[Node] = []
    length: int = 0

    def __init__(self, nodes: List[Node] = None, length: int = 0):
        if nodes is None:
            nodes = []
        self.nodes = nodes
        self.length = length
