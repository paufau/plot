from typing import Union


class Edge:
    start: Union[str, int] = ""
    end: Union[str, int] = ""
    weight: int = 0

    def __init__(self, start: Union[str, int], end: Union[str, int], weight: int):
        self.start = start
        self.end = end
        self.weight = weight
